var express = require('express'),
	router = express.Router(),
	mongoose = require('mongoose'),
	bodyParser = require('body-parser'),
	methodOverride = require('method-override')

//important: configures JSON parser and enables http verb overrides
router.use(bodyParser.urlencoded({extended:true}))
router.use(methodOverride(function(req, res){
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
      }
}))


var getHome = function(req,res) {
	mongoose.model('Blog').find({},function(err,blogs){
		if (err) {
			return console.error(err)
		} else{
			//respond with both html & json, json needs 'Accept:application/json;' in request header
			res.format({
				html: function(){
					res.render(req.url,{
						"title": 'My Nodejs Blog',
						"blogs": blogs
					})
				},
				json: function(){
					res.json(blogs)
				}
			})
		}
	})	
}
router.route('/')
	.get(function(req,res){
		getHome(req,res)
	})
router.route('/home')
	.get(function(req,res){
		getHome(req,res)
	})
router.route('/archive')
	.get(function(req,res){
		mongoose.model('Blog').find({},function(err,blogs){
			if (err) {
				return console.error(err)
			} else {
				res.format({
					html: function() {
						res.render(req.url,{
							'title': 'Blog Archive',
							"blogs": blogs
						})
					},
					json: function() {
						res.json(blogs)
					}
				})
			}
		})
	})


router.route('/add')
	.get(function(req,res){
		res.format({
			html: function(){
				res.render(req.url,{
					"title": 'Add Blog Post'
				})
			}
		})
	})
	.post(function(req,res){
		var name = req.body.name,
			isPublic = req.body.isPublic,
			content = req.body.content
		mongoose.model('Blog').create({
			name: name,
			isPublic: isPublic,
			content: content
		},function(err,blog){
			if (err) {
				res.send('There has been an error uploading your post, please try again')
			} else {
				console.log('POST creating new blog: '+blog)
				res.format({
					html: function(){
						//updates the header (and the address bar)
						res.location('/blogs/home')
						res.redirect('/blogs/home')
					},
					json: function(){
						res.json(blog)
					}
				})
			}
		})
	})
//error handling for invalid blog ids
router.param('id', function(req, res, next, id) {
	mongoose.model('Blog').findById(id, function(err, blog){
		if (err) {
			console.log(id+' was not found')
			res.status(404)
			var err = new Error('Not found')
			err.status = 404
			res.format({
				html: function() {
					next(err)
				},
				json: function() {
					res.json({message: err.status + ' ' + err})
				}
			})
		} else {
			req.id = id
			next()
		}
	})
})

router.route('/:id')
	.get(function(req,res){
		mongoose.model('Blog').findById(req.id, function(err,blog){
			if (err) {
				console.log('GET error: '+err)
			} else {
				console.log('GET retrieving '+blog._id)
				var postDate = blog.postDate.toISOString()
				postDate = postDate.substring(0,postDate.indexOf('T'))
				res.format({
					html: function() {
						res.render(req.url,{
							"postDate": postDate,
							"title": blog.name,
							"blog": blog
						})
					},
					json: function() {
						res.json(blog)
					}
				})
			}
		})
	})

router.route('/:id/upvote')
	.put(function(req,res){
		mongoose.model('Blog').findById(req.id, function(err, blog){
			blog.update({
				upvotes: blog.upvotes + 1
			}, function(err, blog){
				if (err) {
					res.send('There was a problem with the database')
				} else {
					res.format({
						html: function() {
							res.location('/blogs/home')
							res.redirect('/blogs/home')
						},
						json: function() {
							res.json(blog)
						}
					})
				}
			})
		})
	})

router.route('/:id/edit')
	.get(function(req,res){
		mongoose.model('Blog').findById(req.id, function(err, blog){
			if (err) {
				console.log('GET error: '+err)
			} else {
				console.log('GET retrieving '+blog._id)
				var postDate = blog.postDate.toISOString()
				postDate = postDate.substring(0,postDate.indexOf('T'))
				res.format({
					html: function() {
						res.render(req.url,{
							"title": 'Edit Blog '+blog._id,
							"postDate": postDate,
							"blog": blog
						})
					},
					json: function() {
						res.json(blog)
					}
				})
			}
		})
	})
	.put(function(req,res){
		var name = req.body.name,
			isPublic = req.body.isPublic,
			content = req.body.content

		mongoose.model('Blog').findById(req.id, function(err,blog){
			blog.update({
				name: name,
				isPublic: isPublic,
				content: content
			}, function(err, blog){
				if (err) {
					res.send('There was a problem updating the database')
				} else {
					res.format({
						html: function(){
							res.redirect('/blogs/home')
						},
						json: function(){
							res.json(blog)
						}
					})
				}
			})
		})
	})
	.delete(function(req,res){
		mongoose.model('Blog').findById(req.id, function(err,blog){
			if (err) {
				return console.error(err)
			} else {
				blog.remove(function(err, blog){
					if (err) {
						return console.error(err)
					} else {
						res.format({
							html: function(){
								res.redirect('/blogs/home')
							},
							json: function(){
								res.json({
									message: 'Deleted', 
									item: blog
								})
							}
						})
					}
				})
			}
		})
	})

module.exports = router