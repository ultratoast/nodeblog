var express = require('express');
var router = express.Router(),
	React = require('react'),
	ReactDOMServer = require('react-dom/server')

/* GET home page. */
router.get('/', function(req, res, next) {
  	res.render(req.url, { 'title': 'My Universal JS example'});
});

module.exports = router;