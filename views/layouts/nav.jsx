var React = require('react'),
	ReactDOM = require('react-dom'),
	Link = require('react-router/lib/Link')

var SiteNav = React.createClass({
	render: function(){
		return (
			<ul className="site-nav">
				<li><Link to="/">Site Index</Link></li>
				<li><Link to="/blogs/">Blog Index</Link></li>
				<li><Link to="/blogs/home">Blog Home</Link></li>
				<li><Link to="/blogs/archive">Blog Archive</Link></li>
			</ul>
		)
	}	
})

if (typeof module !== 'undefined' && module.exports) {
	module.exports = SiteNav
} else {
	ReactDOM.render(<SiteNav/>,document.getElementById('content'))	
}