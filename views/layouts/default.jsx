var React = require('react'),
	ReactDOM = require('react-dom'),
	SiteNav = require('app/layouts/nav.jsx')

var DefaultLayout = React.createClass({
	render: function() {
		return (
			<html>
				<head>
					<title>{this.props.title}</title>
					<link rel="stylesheet" href="/css/styles.css"/>
				</head>
				<body id="body">
					<SiteNav/>
					<section id="content">{this.props.children}</section>
					<script src="/js/compiled/bundle.js"></script>
				</body>
			</html>
		)
	}
})

if (typeof module !== 'undefined' && module.exports) {
	module.exports = DefaultLayout
} else {
	ReactDOM.render(<DefaultLayout/>,document.getElementById('content'))	
}