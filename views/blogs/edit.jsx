var React = require('react'),
	ReactDOM = require('react-dom'),
	EditForm = require('app/blogs/editform.jsx'),
	DeleteForm = require('app/blogs/deleteform.jsx')



var EditBlog = React.createClass({
	getInitialState: function () {
	  return {blog: this.props.blog}
	},
	render: function() {
		return (
			<div>
				<EditForm blog={this.state.blog}/>
				<DeleteForm blog={this.state.blog}/>
			</div>
		)
	}
})

if (typeof module !== 'undefined' && module.exports) {
	module.exports = EditBlog
} else {
	ReactDOM.render(<EditBlog/>, document.getElementById('content'))	
}