var React = require('react'),
	ReactDOM = require('react-dom'),
	Blog = require('app/blogs/blog.jsx')

var BlogHome = React.createClass({
	getInitialState: function () {
	  return {blogs: this.props.blogs}
	},
	render: function(){
		let blogs = this.state.blogs
		if (blogs && blogs.length > 0) {
			blogs = blogs.map(function(blog,i){
				let isPublic = blog.isPublic ? <Blog key={i} blog={blog}/> : null
				return (
					isPublic
				)
			}.bind(this))
			blogs = blogs.filter(function(blog){
				if (blog) {
					return true
				} else {
					return false
				}
			})
			if (blogs.length > 0) {
				return (
					<div>
						<h2>Blog Home</h2>
						<ul className="blogs">
							{blogs}
						</ul>
					</div>
				)
			} else {
				return (
					<div>
						<h2>Blog Home</h2>
						<p>Sorry, No public blogs found!</p>
					</div>
				)
			}
		} else {
			return (
				<p>Sorry, No data!</p>
			)
		}
	}
})

if (typeof module !== 'undefined' && module.exports) {
	module.exports = BlogHome
} else {
	ReactDOM.render(<BlogHome/>,document.getElementById('content'))	
}