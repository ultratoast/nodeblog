var React = require('react'),
	ReactDOM = require('react-dom')

var DeleteForm = React.createClass({
	getInitialState: function () {
	  return {blog: this.props.blog}
	},
	handleSubmit: function(e) {
		e.preventDefault()
		var blog_id = React.findDOMNode(this.refs.blog_id).value.trim(),
	    	_method = React.findDOMNode(this.refs._method).value.trim()
		if (!blog_id) {
			alert('Missing IDs')
			return
		}
		this.props.onDeleteSubmit({blog_id: blog_id,_method:_method})
		return
	},
	render: function() {
		var blog = this.state.blog
		return (
			<form onSubmit={this.handleSubmit} className="delete-blog" name="deleteblog" method="POST" action={"/blogs/"+blog._id+"/edit"}>
				<input type="hidden" ref="blog_id" name="blog_id"/>
				<input type="hidden" value="DELETE" name="_method" ref="_method"/>
				<button type="submit">Delete Blog</button>
			</form>
		)
	}
})

if (typeof module !== 'undefined' && module.exports) {
	module.exports = DeleteForm
} else {
	ReactDOM.render(<DeleteForm/>, document.getElementById('content'))	
}