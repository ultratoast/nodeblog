var React = require('react'),
	ReactDOM = require('react-dom'),
	Blog = require('app/blogs/blog.jsx')

var BlogArchive = React.createClass({
	getInitialState: function () {
	  return {blogs: this.props.blogs}
	},
	render: function() {
		let blogs = this.state.blogs
		if (blogs && blogs.length > 0) {
			blogs = blogs.map(function(blog,i){
				return (
					<Blog key={i} blog={blog} />
				)
			}.bind(this))
			return (
				<div>
					<h2>Blog Archive</h2>
					<ul className="blogs">
						{blogs}
					</ul>
				</div>
			)
		} else {
			return (
				<div>
					<p>No Blogs Found!</p>
				</div>
			)
		} 
	}
})

if (typeof module !== 'undefined' && module.exports) {
	module.exports = BlogArchive
} else {
	ReactDOM.render(<BlogArchive/>,document.getElementById('content'))	
}