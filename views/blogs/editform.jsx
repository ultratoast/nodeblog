var React = require('react'),
	ReactDOM = require('react-dom')

var EditForm = React.createClass({
	getInitialState: function () {
	  return {blog: this.props.blog}
	},
	handleSubmit: function(e) {
	    e.preventDefault()
	    var name = React.findDOMNode(this.refs.name).value.trim(),
	    	isPublic = React.findDOMNode(this.refs.isPublic).value.trim(),
	    	content = React.findDOMNode(this.refs.content).value.trim(),
	    	_method = React.findDOMNode(this.refs._method).value.trim()
	    if (!content || !name) {
	      return false
	    }
	    this.props.onEditSubmit({name: name,isPublic: isPublic,content: content,_method:_method})
	    // return
	},
	render: function() {
		var blog =this.state.blog
		return (
			<form onSubmit={this.handleSubmit} className="edit-blog" method="POST" name="editblog" action={"/blogs/"+blog._id+"/edit"}>
				<h2>Edit Blog</h2>
				<label>Name</label>
				<input name="name" ref="name" type="text" defaultValue={blog.name}/>
				<br />
				<label>Published</label>
				<input name="isPublic" type="checkbox" ref="isPublic" defaultChecked={blog.isPublic}/>
				<br />
				<label>Content</label>
				<textarea name="content" ref="content" defaultValue={blog.content}></textarea>
				<br />
				<input type="hidden" value="PUT" name="_method" ref="_method"/>
				<button type="submit">Save</button>
			</form>
		)
	}
})

if (typeof module !== 'undefined' && module.exports) {
	module.exports = EditForm
} else {
	ReactDOM.render(<EditForm/>, document.getElementById('content'))	
}