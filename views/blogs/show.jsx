var React = require('react'),
	Link = require('react-router/lib/Link'),
	Blog = require('app/blogs/blog.jsx')

var ShowBlog = React.createClass({
	getInitialState: function () {
	  return {blog: this.props.blog}
	},
	render: function() {
		var blog = this.state.blog
		return (
			<div>
				<Blog blog={blog}/>
				<Link to={"/blogs/"+blog._id+"/edit"}>Edit</Link>
			</div>
		)
	}
})

if (typeof module !== 'undefined' && module.exports) {
	module.exports = ShowBlog
} else {
	React.render(<ShowBlog />,document.getElementById('content'))
}