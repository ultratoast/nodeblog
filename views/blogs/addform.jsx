var React = require('react'),
	ReactDOM = require('react-dom')

var AddForm = React.createClass({
	handleSubmit: function(e) {
	    e.preventDefault()
	    var name = React.findDOMNode(this.refs.name).value.trim(),
	    	isPublic = React.findDOMNode(this.refs.isPublic).value.trim(),
	    	content = React.findDOMNode(this.refs.content).value.trim()
	    if (!content || !name) {
	      return
	    }
	    this.props.onAddSubmit({name: name,isPublic: isPublic,message: message})
	    React.findDOMNode(this.refs.name).value = ''
	    React.findDOMNode(this.refs.isPublic).value = false 
	    React.findDOMNode(this.refs.content).value = ''
	},
	render: function(){
		return (
			<form onSubmit={this.handleSubmit} className="add-blog" method="POST">
				<label>Name</label>
				<input name="name" ref="name" type="text"/>
				<br />
				<label>Published</label>
				<input name="isPublic" type="checkbox" ref="isPublic"/>
				<br />
				<label>Content</label>
				<textarea name="content" ref="content"></textarea>
				<br />
				<button type="submit">Save</button>
			</form>	
		)
	}
})

if (typeof module !== 'undefined' && module.exports) {
	module.exports = AddForm
} else {
	ReactDOM.render(<AddForm/>, document.getElementById('content'))	
}