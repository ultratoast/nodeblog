var React = require('react'),
	ReactDOM = require('react-dom'),
	UpvoteForm = require('app/blogs/upvoteform.jsx')

var Blog = React.createClass({
	getInitialState: function () {
	  return {blog: this.props.blog,postDate: this.props.postDate}
	},
	componentDidMount: function() {
		console.log('mounted!')
	},
	render: function(){
		var blog = this.state.blog,
			postDate = this.state.postDate ? <strong>blog.postDate.toTimeString()</strong> : false
		return (
			<li className="blog">	
				<Link to={"/blogs/"+blog._id}><h1>{blog.name}</h1></Link>
				{postDate}
				<br />
				<span className="upvotes">Upvotes: {blog.upvotes}</span>
				<p>{blog.content}</p>	
				<UpvoteForm blog={blog}/>
			</li>
		)
	}
})

if (typeof module !== 'undefined' && module.exports) {
	module.exports = Blog
} else {
	ReactDOM.render(<Blog/>,document.getElementById('content'))	
}