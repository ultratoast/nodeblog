var React = require('react'),
	ReactDOM = require('react-dom')

var Index = React.createClass({
	getInitialState: function () {
	  return {blogs: this.props.blogs}
	},
	componentDidMount: function(){
	},
	render: function(){
		return (
			<div>
				<h1>My cool Nodejs Blog</h1>
				{this.props.children}
			</div>
		)
	}
})

if (typeof module !== 'undefined' && module.exports) {
	module.exports = Index
} else {
	ReactDOM.render(<Index/>,document.getElementById('content'))	
}