var React = require('react'),
	ReactDOM = require('react-dom')

var UpvoteForm = React.createClass({
	getInitialState: function () {
	  return {blog: this.props.blog}
	},
	handleSubmit: function(e) {
	    e.preventDefault()
	    var _method = React.findDOMNode(this.refs._method).value.trim()
	    this.props.onUpvoteSubmit({_method:_method})
	    // return
	},
	render: function() {
		var blog =this.state.blog
		return (
			<form onSubmit={this.handleSubmit} className="upvote-blog" method="POST" name="upvoteblog" action={"/blogs/"+blog._id+"/upvote"}>
				<input type="hidden" value="PUT" name="_method" ref="_method"/>
				<button type="submit">Upvote</button>
			</form>
		)
	}
})

if (typeof module !== 'undefined' && module.exports) {
	module.exports = UpvoteForm
} else {
	ReactDOM.render(<UpvoteForm/>, document.getElementById('content'))	
}