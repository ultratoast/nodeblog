var React = require('react'),
	ReactDOM = require('react-dom')

var IndexPage = React.createClass({
	render: function(){
		return (
			<p>Welcome Home</p>
		)
	}
})
if (typeof module !== 'undefined' && module.exports) {
	module.exports = IndexPage
} else {
	ReactDOM.render(<IndexPage/>,document.getElementById('content'))	
}