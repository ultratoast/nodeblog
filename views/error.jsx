var React = require('react'),
	ReactDOM = require('react-dom')

var ErrorPage = React.createClass({
	render: function(){
		return (
			<p>{this.props.message}</p>
		)
	}
})
if (typeof module !== 'undefined' && module.exports) {
	module.exports = ErrorPage
} else {
	ReactDOM.render(<ErrorPage/>,document.getElementById('content'))	
}