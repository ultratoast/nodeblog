var React = require('react'),
	Router = require('react-router/lib/Router'),
	Route = require('react-router/lib/Route'),
	browserHistory = require('react-router/lib/browserHistory'),
	IndexRoute = require('react-router/lib/IndexRoute'),
	DefaultLayout = require('app/layouts/default.jsx'),
	Index = require('app/index.jsx'),
	BlogIndex = require('app/blogs/index.jsx'),
	BlogHome = require('app/blogs/home.jsx'),
	BlogArchive = require('app/blogs/archive.jsx'),
	EditBlog = require('app/blogs/edit.jsx'),
	ShowBlog = require('app/blogs/show.jsx'),
	AddForm = require('app/blogs/addform.jsx')

var routes = module.exports = (
	<Router history={browserHistory}>
		<Route path="/" component={DefaultLayout}>
			<IndexRoute component={Index}/>
			<Route path="blogs/" component={BlogIndex}>
				<Route path="home" component={BlogHome}/>
				<Route path="archive" component={BlogArchive}/>
				<Route path=":id/edit" component={EditBlog}/>
				<Route path=":id" component={ShowBlog}/>
				<Route path="add" component={AddForm}/>
			</Route>
		</Route>
	</Router>
)