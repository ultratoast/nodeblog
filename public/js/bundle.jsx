var Client = require('modules/react-engine/lib/client'),
	Routes = require('./routes.jsx'),
	options = {
		routes: Routes,
		viewResolver: function(viewName) {
			return require('app/'+viewName)
		}
	}
document.addEventListener('DOMContentLoaded', function onLoad(){
	Client.boot(options, function onBoot(data,history){

	})
})
