preferred_syntax = :sass
http_path = 'public/'
css_dir = 'public/css'
sass_dir = 'public/scss'
images_dir = 'public/img'
javascripts_dir = 'public/js'
relative_assets = true
output_style = :compressed